CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Contact

INTRODUCTION
------------
There could be requirement of something like configuring the grading system for
universities. Then there is a need to set the configuration for the grading
system per country.

The Grade system by country module provides option to configure the grade system
by country.

INSTALLATION
------------
* Install as usual,
see https://www.drupal.org/documentation/install/modules-themes/modules-7.

CONFIGURATION
-------------
*
*

CONTACT
-------
Current maintainers:
  * Naveen Valecha (naveenvalecha) - http://drupal.org/u/naveenvalecha
